<?php
$template_directory = get_template_directory_uri() . "/img/";
?>

<?php
if (have_posts()) :
    query_posts(array('category_name'  => 'rodape', 'posts_per_page' => 1));
    while (have_posts()) : the_post();
?>


        <div class="container fonte-cinza fonte-size24">
            <div class="row">
                <div class="col s12">
                    <div class="col s12 m6">
                        <div class="row negrito">Endereço</div>


                        <?php
                        $link = get_field('mapa');
                        if ($link) :
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                        ?>
                            <a class="button" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?>
                            <?php endif; ?>

                            <div class="row ">
                            <?php if(get_field('rua')){ the_field('rua');} ?>, <?php if(get_field('numero')){ the_field('numero');} ?>, <?php if(get_field('cep')){ the_field('cep');} ?>, <?php if(get_field('bairro')){ the_field('bairro');} ?>  
                            </div>
                            <?php if ($link) : ?>
                            </a>
                        <?php endif; ?>
                        <div class="row "><?php if(get_field('cidade')){ the_field('cidade');} ?> - <?php if(get_field('estado')){ the_field('estado');} ?></div>
                        <div class="row ">
                            <a href="<?php echo get_site_url(); ?>" class="brand-logo">
                                <img class="logo logo-rodape" src="<?php echo $template_directory . 'logo.png' ?>" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col s12 m4">
                        <div class="row negrito">Contato</div>
                        <div class="row"><?php if(get_field('contato_1')){ the_field('contato_1');} ?></div>
                        <div class="row"><?php if(get_field('contato_2')){ the_field('contato_2');} ?></div>
                        <div class="row"><?php if(get_field('email_1')){ the_field('email_1');} ?></div>
                        <div class="row"><?php if(get_field('email_2')){ the_field('email_2');} ?></div>
                    </div>
                    <div class="col s2">
                        <div class="row negrito">Menu</div>
                        <div class="row menu-rodape">
                            <?php wp_nav_menu(
                                array(
                                    'theme_location' => 'bottom_menu'
                                )
                            ); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    <?php
    endwhile;
else :
    ?>
    <p>Nenhum imagem cadastrado !</p>
<?php
endif;
?>


<div class="fundo-cinza-escuro rodape-copy">
    <div class="container fonte-branca">
        <div class="row margin-bottom-20">
            <div class="col s7">
                <?php $ano = date('Y'); ?>
                <p>Copyright © Refratale Serviços Eireli - EPP <?php echo $ano; ?>. Todos os direitios reservados. </p>
            </div>
            <div class="col s5 right-align">
                <p>Desenvolvido por: <a href="https://instagram.com/agencia_leva" target="_blank"><img class="logo-leva logo-rodape" src="<?php echo $template_directory . 'leva.png' ?>" alt=""></a></p>
            </div>
        </div>
    </div>
</div>

</body>

</html>